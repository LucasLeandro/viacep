﻿using ConsumindoViaCep.View;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ConsumindoViaCep
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new ListagemViewCep());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
